# About
Small vaadin crud for tracking completed games and custom modifications/romhacks for games with it. Design looks bad, but app actually works.
Mostly written because i liked Vaadin framework(because you can develop frontend only with java code).

## Setting up
Simple and easy. Just get code, install any sql database(i used postgres), add hibernate.cfg.xml with db settings and deploy build to any java web server(tomcat or glassfish, or anything else you like).

## Db Schema
You need table "mods" in your db. In this table there must be columns:
* string name (which is id)
* string about
* string image
* string link
* string game
* long date (unix timestamp)

## Used tools
Spring + Hibernate(classic!) and Vaadin of course.