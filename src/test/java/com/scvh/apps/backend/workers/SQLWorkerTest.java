package com.scvh.apps.backend.workers;


import com.scvh.apps.backend.abstractions.Mod;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Test;

public class SQLWorkerTest {

    private SQLWorker worker;

    public SQLWorkerTest() {
        SessionFactory factory = new Configuration().configure().addAnnotatedClass(Mod.class)
                .buildSessionFactory();
        worker = new SQLWorker(factory);
    }

    @Test
    public void testSavingAndReading() {
        Mod mod = new Mod();
        mod.setName("testName");
        mod.setAbout("test");
        worker.writeToDatabase(mod);
        Mod gotMod = worker.readFromDatabase("testName");
        Assert.assertEquals("test", gotMod.getAbout());
    }

    @Test
    public void testCounting() {
        Mod mod = new Mod();
        mod.setName("test");
        Mod mod1 = new Mod();
        mod1.setName("test1");
        Mod mod2 = new Mod();
        mod2.setName("test2");
        worker.writeToDatabase(mod);
        worker.writeToDatabase(mod1);
        worker.writeToDatabase(mod2);
        Assert.assertEquals(3, worker.getNumberOfMods());
    }

}
