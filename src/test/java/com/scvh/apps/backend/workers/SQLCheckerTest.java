package com.scvh.apps.backend.workers;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.spring.di.ApplicationSqlContext;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SQLCheckerTest {

    private SQLChecker checker;
    private SQLWorker worker;

    public SQLCheckerTest() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationSqlContext
                .class);
        checker = (SQLChecker) context.getBean("sqlChecker");
        worker = (SQLWorker) context.getBean("sqlWorker");
    }

    @Test
    public void check() {
        Mod mod = new Mod();
        mod.setName("test");
        worker.writeToDatabase(mod);
        Assert.assertEquals(true, checker.isThisModExists("test"));
    }
}
