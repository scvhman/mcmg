package com.scvh.apps.backend.crudlayer;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.workers.SQLWorker;

import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class ModLoaderTest {

    private SQLWorker worker;
    private ModLoader loader;

    public ModLoaderTest() {
        worker = new SQLWorker(new Configuration().configure().addAnnotatedClass(Mod.class)
                .buildSessionFactory());
        loader = new ModLoader(worker);
    }

    @Test
    public void testLoading() {
        Mod mod1 = new Mod();
        mod1.setName("test1");
        Mod mod2 = new Mod();
        mod2.setName("test2");
        worker.writeToDatabase(mod1);
        worker.writeToDatabase(mod2);
        ArrayList<Mod> mods = loader.getListOfMods();
        Assert.assertEquals(mod2.getName(), mods.get(1).getName());
    }
}
