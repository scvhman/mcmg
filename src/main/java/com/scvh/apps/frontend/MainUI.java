package com.scvh.apps.frontend;

import com.scvh.apps.backend.crudlayer.ModLoader;
import com.scvh.apps.backend.workers.CoverSaver;
import com.scvh.apps.frontend.factories.BasicHTMLElementsFactory;
import com.scvh.apps.frontend.pages.AddNewModPage;
import com.scvh.apps.frontend.pages.EditModPage;
import com.scvh.apps.frontend.pages.MainPage;
import com.scvh.apps.frontend.pages.ModInfoPage;
import com.scvh.apps.spring.di.ApplicationSqlContext;
import com.scvh.apps.spring.di.ApplicationWorkersContext;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.annotation.WebServlet;

@Theme("valo")
public class MainUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        initPages(new Navigator(this, this));
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MainUI.class, productionMode = false)
    public static class MainServlet extends VaadinServlet {
        //dull method
    }

    private void initPages(Navigator navigator) {
        //no DI here so big start
        ApplicationContext sqlContext = new AnnotationConfigApplicationContext
                (ApplicationSqlContext
                        .class);
        ApplicationContext workersContext = new AnnotationConfigApplicationContext
                (ApplicationWorkersContext
                        .class);
        BasicHTMLElementsFactory htmlElementsFactory = new BasicHTMLElementsFactory(navigator,
                (CoverSaver) workersContext.getBean("receiver"));
        MainPage mainPage = new MainPage(htmlElementsFactory, (ModLoader) sqlContext.getBean
                ("modLoader"));
        AddNewModPage newModPage = new AddNewModPage(htmlElementsFactory);
        ModInfoPage infoPage = new ModInfoPage(htmlElementsFactory, (ModLoader) sqlContext
                .getBean("modLoader"));
        EditModPage modPage = new EditModPage(htmlElementsFactory, (ModLoader) sqlContext.getBean
                ("modLoader"));
        navigator.addView("main", mainPage);
        navigator.addView("add", newModPage);
        navigator.addView("mod", infoPage);
        navigator.addView("edit", modPage);
        setContent(mainPage);
    }

}
