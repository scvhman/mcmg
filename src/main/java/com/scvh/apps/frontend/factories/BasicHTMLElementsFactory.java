package com.scvh.apps.frontend.factories;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.crudlayer.ModEditor;
import com.scvh.apps.backend.crudlayer.ModRemover;
import com.scvh.apps.backend.crudlayer.ModSaver;
import com.scvh.apps.backend.workers.CoverSaver;
import com.scvh.apps.spring.di.ApplicationSqlContext;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FileResource;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.File;
import java.time.LocalDate;

public class BasicHTMLElementsFactory {

    private ButtonClickListenerSetter listener;
    private CoverSaver saver;

    public BasicHTMLElementsFactory(Navigator navigator, CoverSaver saver) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationSqlContext
                .class);
        listener = new ButtonClickListenerSetter(navigator, (ModSaver) context.getBean
                ("modSaver"), (ModRemover) context.getBean("remover"), (ModEditor) context
                .getBean("editor"));
        this.saver = saver;
    }

    public HorizontalLayout getHeader() {
        HorizontalLayout header = new HorizontalLayout();
        Label label = new Label("placeholder");
        label.setWidth(70, Sizeable.Unit.PERCENTAGE);
        Button mainPage = new Button("Mods");
        mainPage.setWidth(15, Sizeable.Unit.PERCENTAGE);
        listener.setClickListenerForHomeButton(mainPage);
        Button add = new Button("Add");
        add.setWidth(15, Sizeable.Unit.PERCENTAGE);
        listener.setClickListenerForAddButton(add);
        header.addComponent(label);
        header.addComponent(mainPage);
        header.addComponent(add);
        return header;
    }

    public GridLayout getModGrid() {
        GridLayout gridLayout = new GridLayout();
        gridLayout.setColumns(5);
        gridLayout.setSizeFull();
        return gridLayout;
    }

    public FormLayout getFieldsForNewMod() {
        FormLayout layout = new FormLayout();
        TextField name = new TextField("Name");
        TextField about = new TextField("About");
        TextField link = new TextField("Link");
        Upload coverUpload = new Upload("Upload cover", saver);
        coverUpload.addFinishedListener((Upload.FinishedListener) finishedEvent -> listener
                .setFileLinkForImage(saver.getFileLink()));
        DateField year = new DateField("Release date");
        TextField game = new TextField("Game");
        Button button = new Button("Submit");
        name.setPlaceholder("Mod name");
        about.setPlaceholder("About mod");
        link.setPlaceholder("Mod site");
        year.setValue(LocalDate.now());
        game.setPlaceholder("Base game");
        layout.addComponent(name);
        layout.addComponent(about);
        layout.addComponent(coverUpload);
        layout.addComponent(link);
        layout.addComponent(year);
        layout.addComponent(game);
        layout.addComponent(button);
        listener.setClickListenerForSubmitButton(button, layout);
        return layout;
    }

    public FormLayout modEditing(Mod mod) {
        FormLayout layout = new FormLayout();
        TextField name = new TextField("Name");
        TextField about = new TextField("About");
        Upload coverUpload = new Upload("Upload cover", saver);
        coverUpload.addFinishedListener((Upload.FinishedListener) finishedEvent -> listener
                .setFileLinkForImage(saver.getFileLink()));
        TextField link = new TextField("Link");
        DateField year = new DateField("Release date");
        TextField game = new TextField("Game");
        Button button = new Button("Save");
        name.setValue(mod.getName());
        if (mod.getAbout() != null) {
            about.setValue(mod.getAbout());
        }
        if (mod.getLink() != null) {
            link.setValue(mod.getLink());
        }
        year.setValue(mod.getDateInLocalDate());
        if (mod.getGame() != null) {
            game.setValue(mod.getGame());
        }
        listener.setClickListenerForSaveEditedButton(button, layout, mod);
        layout.addComponent(name);
        layout.addComponent(about);
        layout.addComponent(coverUpload);
        layout.addComponent(link);
        layout.addComponent(year);
        layout.addComponent(game);
        layout.addComponent(button);
        return layout;
    }

    public Panel getModUIRepresentation(Mod mod) {
        Panel panel = new Panel(mod.getName());
        if (mod.getImage() != null) {
            Image image = new Image("Cover", new FileResource(new File(mod.getImage())));
            panel.setContent(image);
        }
        listener.setClickListenerForMod(panel, mod);
        return panel;
    }

    public HorizontalLayout getModInfo(Mod mod) {
        HorizontalLayout modInfo = new HorizontalLayout();
        modInfo.addComponent(new Label(mod.getName()));
        Button deleteButton = new Button("Delete");
        Button editButton = new Button("Edit");
        Button removeImageButton = new Button("Remove image");
        modInfo.addComponent(deleteButton);
        modInfo.addComponent(editButton);
        modInfo.addComponent(removeImageButton);
        listener.setClickListenerForDeleteButton(deleteButton, mod);
        listener.setClickListenerForEditButton(editButton, mod);
        listener.setClickListenerForRemoveImageButton(removeImageButton, mod);
        return modInfo;
    }
}
