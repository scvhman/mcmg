package com.scvh.apps.frontend.factories;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.crudlayer.ModEditor;
import com.scvh.apps.backend.crudlayer.ModRemover;
import com.scvh.apps.backend.crudlayer.ModSaver;
import com.vaadin.event.MouseEvents;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;

class ButtonClickListenerSetter {

    private Navigator navigator;
    private ModSaver saver;
    private ModRemover remover;
    private ModEditor editor;

    private String linkFlag;

    ButtonClickListenerSetter(Navigator navigator, ModSaver saver, ModRemover remover, ModEditor
            editor) {
        this.navigator = navigator;
        this.saver = saver;
        this.remover = remover;
        this.editor = editor;
    }

    void setClickListenerForAddButton(Button button) {
        button.addClickListener((Button.ClickListener) clickEvent -> navigator.navigateTo("add"));
    }

    void setClickListenerForHomeButton(Button button) {
        button.addClickListener((Button.ClickListener) clickEvent -> navigator.navigateTo("main"));
    }

    void setClickListenerForSubmitButton(Button button, FormLayout layout) {
        button.addClickListener((Button.ClickListener) clickEvent -> {
            saver.saveMod(layout, linkFlag);
            linkFlag = null;
            navigator.navigateTo("main");
        });
    }

    void setClickListenerForMod(Panel panel, Mod mod) {
        panel.addClickListener((MouseEvents.ClickListener) clickEvent -> navigator.navigateTo
                ("mod/" + mod.getName()));
    }

    void setClickListenerForDeleteButton(Button button, Mod mod) {
        button.addClickListener((Button.ClickListener) clickEvent -> {
            remover.removeThisMod(mod);
            navigator.navigateTo("main");
        });
    }

    void setClickListenerForEditButton(Button button, Mod mod) {
        button.addClickListener((Button.ClickListener) clickEvent -> navigator.navigateTo("edit/"
                + mod.getName()));
    }

    void setClickListenerForSaveEditedButton(Button button, FormLayout layout, Mod oldMod) {
        button.addClickListener((Button.ClickListener) clickEvent -> {
            editor.editMod(layout, oldMod, linkFlag);
            linkFlag = null;
            navigator.navigateTo("main");
        });
    }

    void setClickListenerForRemoveImageButton(Button button, Mod mod) {
        button.addClickListener((Button.ClickListener) clickEvent -> {
            editor.removeImage(mod);
            navigator.navigateTo("main");
        });
    }

    void setFileLinkForImage(String link) {
        this.linkFlag = link;
    }
}
