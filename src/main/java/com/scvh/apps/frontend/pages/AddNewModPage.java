package com.scvh.apps.frontend.pages;

import com.scvh.apps.frontend.factories.BasicHTMLElementsFactory;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.VerticalLayout;

@DesignRoot
public class AddNewModPage extends VerticalLayout implements View {

    private BasicHTMLElementsFactory elementsFactory;

    public AddNewModPage(BasicHTMLElementsFactory elementsFactory) {
        this.elementsFactory = elementsFactory;
        addComponent(getLayout());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    private VerticalLayout getLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(elementsFactory.getHeader());
        layout.addComponent(elementsFactory.getFieldsForNewMod());
        return layout;
    }
}
