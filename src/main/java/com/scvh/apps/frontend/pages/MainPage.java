package com.scvh.apps.frontend.pages;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.crudlayer.ModLoader;
import com.scvh.apps.frontend.factories.BasicHTMLElementsFactory;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.VerticalLayout;

import java.util.ArrayList;
import java.util.Iterator;

@DesignRoot
public class MainPage extends VerticalLayout implements View {

    private BasicHTMLElementsFactory elementsFactory;
    private ModLoader loader;
    private GridLayout modGrid;

    public MainPage(BasicHTMLElementsFactory elementsFactory, ModLoader loader) {
        this.elementsFactory = elementsFactory;
        this.loader = loader;
        populateUI();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        populateUI();
    }

    private VerticalLayout getLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(elementsFactory.getHeader());
        GridLayout modGrid = elementsFactory.getModGrid();
        this.modGrid = modGrid;
        layout.addComponent(modGrid);
        return layout;
    }

    private void populateUI() {
        removeAllComponents();
        addComponent(getLayout());
        ArrayList<Mod> mods = loader.getListOfMods();
        Iterator<Mod> modIterator = mods.iterator();
        while (modIterator.hasNext()) {
            modGrid.addComponent(elementsFactory.getModUIRepresentation(modIterator.next()));
        }
    }

}
