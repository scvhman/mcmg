package com.scvh.apps.frontend.pages;

import com.scvh.apps.backend.crudlayer.ModLoader;
import com.scvh.apps.frontend.factories.BasicHTMLElementsFactory;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.VerticalLayout;

public class ModInfoPage extends VerticalLayout implements View {

    private BasicHTMLElementsFactory htmlElementsFactory;
    private ModLoader modLoader;

    public ModInfoPage(BasicHTMLElementsFactory htmlElementsFactory, ModLoader modLoader) {
        this.htmlElementsFactory = htmlElementsFactory;
        this.modLoader = modLoader;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        refreshUI(viewChangeEvent.getParameters().split("/")[0]);
    }

    private void refreshUI(String modKey) {
        removeAllComponents();
        addComponent(htmlElementsFactory.getHeader());
        addComponent(htmlElementsFactory.getModInfo(modLoader.getMod(modKey)));
    }
}
