package com.scvh.apps.frontend.pages;

import com.scvh.apps.backend.crudlayer.ModLoader;
import com.scvh.apps.frontend.factories.BasicHTMLElementsFactory;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.VerticalLayout;

public class EditModPage extends VerticalLayout implements View {

    private ModLoader loader;
    private BasicHTMLElementsFactory elementsFactory;

    public EditModPage(BasicHTMLElementsFactory elementsFactory, ModLoader loader) {
        this.elementsFactory = elementsFactory;
        this.loader = loader;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        populateUI(viewChangeEvent.getParameters().split("/")[0]);
    }

    private void populateUI(String modKey) {
        removeAllComponents();
        addComponent(elementsFactory.getHeader());
        addComponent(elementsFactory.modEditing(loader.getMod(modKey)));
    }
}
