package com.scvh.apps.backend.workers;

import com.scvh.apps.backend.abstractions.Mod;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

import java.util.ArrayList;

public class SQLWorker {

    private SessionFactory factory;

    public SQLWorker(SessionFactory factory) {
        this.factory = factory;
    }

    public void writeToDatabase(Mod mod) {
        Session modWriter = factory.openSession();
        Transaction transaction = modWriter.beginTransaction();
        modWriter.save(mod);
        transaction.commit();
    }

    public Mod readFromDatabase(String name) {
        Session modReader = factory.openSession();
        Transaction transaction = modReader.beginTransaction();
        Mod mod = modReader.get(Mod.class, name);
        transaction.commit();
        return mod;
    }

    public void removeMod(Mod mod) {
        Session modRemover = factory.openSession();
        Transaction transaction = modRemover.beginTransaction();
        modRemover.remove(mod);
        transaction.commit();
    }

    public int getNumberOfMods() {
        Session session = factory.openSession();
        //deprecated method
        Number n = (Number) session.createCriteria(Mod.class).setProjection(Projections.rowCount
                ()).uniqueResult();
        return n.intValue();
    }

    public ArrayList<Mod> getAllMods() {
        Session session = factory.openSession();
        return (ArrayList<Mod>) session.createCriteria(Mod.class).list();
    }
}
