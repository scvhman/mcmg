package com.scvh.apps.backend.workers;

import com.vaadin.ui.Upload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CoverSaver implements Upload.Receiver {

    private String fileLink;

    @Override
    public OutputStream receiveUpload(String s, String s1) {
        FileOutputStream stream = null;
        File image = new File(System.getProperty("user.home") + "/covers/" + s);
        if (!image.exists()) {
            try {
                image.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fileLink = image.getAbsolutePath();
        try {
            stream = new FileOutputStream(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void cleanFileLink() {
        fileLink = null;
    }
}
