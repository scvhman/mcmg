package com.scvh.apps.backend.workers;

public class SQLChecker {

    private SQLWorker worker;

    public SQLChecker(SQLWorker worker) {
        this.worker = worker;
    }

    public boolean isThisModExists(String fieldName) {
        return worker.readFromDatabase(fieldName) != null;
    }
}
