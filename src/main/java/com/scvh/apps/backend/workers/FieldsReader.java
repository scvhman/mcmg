package com.scvh.apps.backend.workers;

import com.scvh.apps.backend.abstractions.Mod;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

public class FieldsReader {

    private Binder<Mod> bindFields(FormLayout layout) {
        Binder<Mod> binder = new Binder<>();
        binder.forField((TextField) layout.getComponent(0)).bind(Mod::getName,
                Mod::setName);
        binder.forField((TextField) layout.getComponent(1)).bind(Mod::getAbout, Mod::setAbout);
        binder.forField((TextField) layout.getComponent(3)).bind(Mod::getLink, Mod::setLink);
        binder.forField((DateField) layout.getComponent(4)).bind(Mod::getDateInLocalDate,
                Mod::setDate);
        binder.forField((TextField) layout.getComponent(5)).bind(Mod::getGame, Mod::setGame);
        return binder;
    }

    public Mod getModFromLayout(FormLayout layout) {
        Binder<Mod> binder = bindFields(layout);
        Mod mod = new Mod();
        try {
            binder.writeBean(mod);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        return mod;
    }

}
