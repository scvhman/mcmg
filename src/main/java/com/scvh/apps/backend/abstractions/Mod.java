package com.scvh.apps.backend.abstractions;

import java.time.LocalDate;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mods")
public class Mod {

    @Id
    @Column(name = "name", unique = true, nullable = false)
    private String name;
    @Column(name = "about")
    private String about;
    @Column(name = "image")
    private String image;
    @Column(name = "link")
    private String link;
    @Column(name = "game")
    private String game;
    @Column(name = "date")
    private long date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getDate() {
        return date;
    }

    public LocalDate getDateInLocalDate() {
        return LocalDate.ofEpochDay(date / 86400);
    }

    public void setDate(LocalDate date) {
        this.date = date.atStartOfDay(ZoneId.of("Etc/UTC")).toEpochSecond();
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + about.hashCode() + image.hashCode();
    }
}
