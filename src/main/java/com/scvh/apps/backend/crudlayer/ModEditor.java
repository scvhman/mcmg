package com.scvh.apps.backend.crudlayer;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.workers.FieldsReader;
import com.scvh.apps.backend.workers.SQLWorker;
import com.vaadin.ui.FormLayout;

public class ModEditor {

    private SQLWorker worker;
    private FieldsReader reader;

    public ModEditor(SQLWorker worker, FieldsReader reader) {
        this.worker = worker;
        this.reader = reader;
    }

    private void editMod(Mod oldMod, Mod newMod) {
        worker.removeMod(oldMod);
        worker.writeToDatabase(newMod);
    }

    public void editMod(FormLayout layout, Mod oldMod, String imageLink) {
        Mod mod = reader.getModFromLayout(layout);
        mod.setImage(imageLink);
        editMod(oldMod, mod);
    }

    public void removeImage(Mod mod) {
        worker.removeMod(mod);
        mod.setImage(null);
        worker.writeToDatabase(mod);
    }
}
