package com.scvh.apps.backend.crudlayer;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.workers.SQLWorker;

public class ModRemover {

    private SQLWorker worker;

    public ModRemover(SQLWorker worker) {
        this.worker = worker;
    }

    public void removeThisMod(Mod mod) {
        worker.removeMod(mod);
    }
}
