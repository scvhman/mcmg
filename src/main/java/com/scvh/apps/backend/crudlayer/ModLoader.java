package com.scvh.apps.backend.crudlayer;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.workers.SQLWorker;

import java.util.ArrayList;

public class ModLoader {

    private SQLWorker worker;

    public ModLoader(SQLWorker worker) {
        this.worker = worker;
    }

    public ArrayList<Mod> getListOfMods() {
        return worker.getAllMods();
    }

    public long getNumberOfMods() {
        return worker.getNumberOfMods();
    }

    public Mod getMod(String key) {
        return worker.readFromDatabase(key);
    }

}
