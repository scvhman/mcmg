package com.scvh.apps.backend.crudlayer;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.workers.FieldsReader;
import com.scvh.apps.backend.workers.SQLWorker;
import com.vaadin.ui.FormLayout;

public class ModSaver {

    private FieldsReader reader;
    private SQLWorker worker;

    public ModSaver(FieldsReader reader, SQLWorker worker) {
        this.reader = reader;
        this.worker = worker;
    }

    public void saveMod(FormLayout layout, String fileLink) {
        Mod mod = reader.getModFromLayout(layout);
        mod.setImage(fileLink);
        worker.writeToDatabase(mod);
    }
}
