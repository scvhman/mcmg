package com.scvh.apps.spring.di;

import com.scvh.apps.backend.workers.CoverSaver;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class ApplicationWorkersContext {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CoverSaver receiver() {
        return new CoverSaver();
    }

}
