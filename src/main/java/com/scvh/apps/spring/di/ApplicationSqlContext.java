package com.scvh.apps.spring.di;

import com.scvh.apps.backend.abstractions.Mod;
import com.scvh.apps.backend.crudlayer.ModEditor;
import com.scvh.apps.backend.crudlayer.ModLoader;
import com.scvh.apps.backend.crudlayer.ModRemover;
import com.scvh.apps.backend.crudlayer.ModSaver;
import com.scvh.apps.backend.workers.FieldsReader;
import com.scvh.apps.backend.workers.SQLChecker;
import com.scvh.apps.backend.workers.SQLWorker;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class ApplicationSqlContext {

    private ApplicationContext frontendContext;

    public ApplicationSqlContext() {
        frontendContext = new AnnotationConfigApplicationContext(ApplicationFrontendContext.class);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public SessionFactory sessionFactory() {
        return new Configuration().configure().addAnnotatedClass(Mod.class)
                .buildSessionFactory();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public SQLWorker sqlWorker() {
        return new SQLWorker(sessionFactory());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public SQLChecker sqlChecker() {
        return new SQLChecker(sqlWorker());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ModSaver modSaver() {
        return new ModSaver((FieldsReader) frontendContext.getBean("fieldsReader"), sqlWorker());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ModLoader modLoader() {
        return new ModLoader(sqlWorker());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ModRemover remover() {
        return new ModRemover(sqlWorker());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ModEditor editor() {
        return new ModEditor(sqlWorker(), (FieldsReader) frontendContext.getBean("fieldsReader"));
    }
}
