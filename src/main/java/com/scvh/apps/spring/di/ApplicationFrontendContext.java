package com.scvh.apps.spring.di;

import com.scvh.apps.backend.workers.FieldsReader;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class ApplicationFrontendContext {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public FieldsReader fieldsReader() {
        return new FieldsReader();
    }


}
